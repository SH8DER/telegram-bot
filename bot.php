<?php

include __DIR__ . '/vendor/autoload.php';
include __DIR__ . '/libs/rb-mysql.php';
include __DIR__ . '/functions.php';
include __DIR__ . '/customCommands.php';

use Telegram\Bot\Api;

const IS_LOCALHOST = false;
define("ACCESS_LOG", "access");
define("ERROR_LOG", "error");

$kineuBot = new Api(getKineuBotToken()); // получаем данные которые пришли от телеграмма
$chat_id = $kineuBot->getWebhookUpdates()->getMessage()->getChat()->getId();
$userData = $kineuBot->getWebhookUpdates()->getMessage()->getFrom()->getRawResponse(); // получаем профиль пользователя
$messageEntities = $kineuBot->getWebhookUpdates()->getMessage()->getRawResponse()['entities'][0]; // получаем профиль пользователя
logFile("($chat_id) Получены данные от Telegram", ACCESS_LOG);
$kineuBot->sendChatAction([
    'chat_id' => $chat_id,
    'action' => 'typing',
]);

$dbConectData = getDatabaseConnectData(); // получаем данные для соединения с базой
R::setup("mysql:host={$dbConectData['host']};dbname={$dbConectData['dbname']}", $dbConectData['dbUser'], $dbConectData['dbUserPass']); // устанавливаем соединение с базой
R::freeze(!IS_LOCALHOST); // включаем\отключаем автоматическое ообновление структуры базы
R::fancyDebug(IS_LOCALHOST); // включаем\отключаем дебаг на локалке
$db_connected = R::testConnection();
if (!$db_connected) {
    logFile("Нет соеденения с базой, возможно ошибка в авторизационных данных", ERROR_LOG);
}

if ($db_connected) {

    if (!R::count('users') || empty(R::find('users', "telegram_id = {$userData['id']}"))) {
        $user = R::dispense('users');
        // $user = R::findOrDispense('users', "telegram_id = {$userData['id']}");

        $user->telegram_id = $userData['id'];
        $user->is_bot = $userData['is_bot'];
        $user->first_name = $userData['first_name'];
        $user->username = $userData['username'];
        $user->lang_code = $userData['language_code'];

        $idOfSaveUser = R::store($user);
        logFile("($chat_id) Пользователь {$userData['first_name']} ({$userData['username']}) добавлен в базу данных", ACCESS_LOG);
    }

}

// если есть доплнение к присланному сообщению, и если тип сообщения пользователя содержит команду для бота
if (!empty($messageEntities) && $messageEntities['type'] === 'bot_command') {
    $input_user_text = $kineuBot->getWebhookUpdates()->getMessage()->getText();
    $input_bot_command = getBotCommand($input_user_text, $messageEntities['offset'], $messageEntities['length']);

    runCommand($input_bot_command);
} else {

    $input_user_text = $kineuBot->getWebhookUpdates()->getMessage()->getText();
    switch ($input_user_text) {
        case 'Я хочу узнать об Университете':
            runCommand('menuLv2University');
            break;
        case 'Я хочу узнать о Колледже':
            runCommand('menuLv2College');
            break;
        default:
            if (checkNumberText($input_user_text)) {
                getAnswer($input_user_text);
            } elseif ($input_user_text === "Назад") {
                runCommand('mainMenu');
            } else {
                runCommand('iDontKnow');
            }
            break;
    }
}

if ($db_connected) {
    R::close();
}
// Закрываем соединение с базой, на всякий случай...
