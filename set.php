<?php
require_once "functions.php";
define("SERVER", "kineu.kz");

if (isset($_SERVER['SERVER_NAME'])) {
    if ($_SERVER['SERVER_NAME'] === SERVER) {
        $path = $_SERVER['SERVER_NAME'] . "/telegram-bot/" . array_reverse(explode("/", __DIR__))[0] . "/bot.php";
        $hook_url = "https://$path";
    }
} else {
    $ngrok_tunnels = file_get_contents("http://localhost:4040/api/tunnels");
    if (isJson($ngrok_tunnels)) {
        $ngrok_tunnels = json_decode($ngrok_tunnels);
        $hook_url = $ngrok_tunnels->tunnels[1]->public_url;
    }
}

$token = getKineuBotToken();

$req_url = "https://api.telegram.org/bot$token/setWebhook?url=$hook_url/bot.php";

if (!empty($hook_url)) {
    $result = file_get_contents($req_url);
    logFile("Webhook был установлен - $result", 'webhook.set');
    var_dump($result);
} else {
    logFile('Webhook не был установлен потому что нет ссылки для его установки', 'webhook.set');
}
