<?php

include __DIR__ . '/libs/rb-mysql.php';
include __DIR__ . '/functions.php';

const IS_LOCALHOST = true;
define("ACCESS_LOG", "sending-messages.access");
define("ERROR_LOG", "sending-messages.error");
$bot_token = getKineuBotToken();

function primitiveSendMessage($chat_id, $message_text)
{
    global $bot_token;

    // $message_text = $message_text . "id на которые отправляем $chat_id";
    // $fake_id = 262425935;

    $base_url = "https://api.telegram.org/bot$bot_token/sendMessage?chat_id=$chat_id&text=$message_text&disable_web_page_preview=false";

    $resp = file_get_contents($base_url);

    if (is_null($resp)) {
        logFile("Не получилось отправить сообщения пользователю с telegram_id($chat_id), скорее всего пользователь заблокировал бота", ERROR_LOG);
    } else {
        logFile("Сообщение для ($chat_id) отправлено на сервера telegram, ответ сервера - $resp", ACCESS_LOG);
    }

}

function sendingAll($users, $message_text)
{

    $c = 0;

    foreach ($users as $user) {

        $c++;
        if ($c === 20) {
            sleep(1);
            $c = 0;
        }

        // logFile("Делаем вид что отправляем сообщение пользователю $user", ACCESS_LOG);
        primitiveSendMessage($user, $message_text);

    }

    return true;

}

/*
if ( isset($_GET['mailing_id']) ) {
$mailing_id = (int) trim($_GET['mailing_id']);
if ( !$mailing_id ) {
logFile("ID рассылки был равен 0, возможно в параметры пришла строка вместо цифры, в связи с этим скрипт завершён", ERROR_LOG);
exit("ID рассылки был равен 0, возможно в параметры пришла строка вместо цифры");
}
} else {
logFile("Не был передан ID рассылки, рассылка не запущена", ERROR_LOG);
exit("Не был передан ID рассылки, рассылка не запущена, скрипт завершён");
}
 */

$dbConectData = getDatabaseConnectData(); // получаем данные для соединения с базой
R::setup("mysql:host={$dbConectData['host']};dbname={$dbConectData['dbname']}", $dbConectData['dbUser'], $dbConectData['dbUserPass']); // устанавливаем соединение с базой
R::freeze(!IS_LOCALHOST); // включаем\отключаем автоматическое ообновление структуры базы
R::fancyDebug(IS_LOCALHOST); // включаем\отключаем дебаг на локалке

$db_connected = R::testConnection();
if (!$db_connected) {
    logFile("Нет соеденения с базой, возможно ошибка в авторизационных данных", ERROR_LOG);
    exit("Нет соеденения с базой, возможно ошибка в авторизационных данных");
} else {

    $users_for_mailing = array();
    $mailing = R::findOne("mailings", "status = 0");

    if (!$mailing->status) {

        $countUsersFormMailing = R::findAll("users", "LIMIT 100 OFFSET {$mailing->offset}");
        if (count($countUsersFormMailing) === 0) {
            $mailing->status = 1;
            R::store($mailing);
            logFile("Полностью завершена рассылка c id = {$mailing->id}", ACCESS_LOG);
            exit("Полностью завершена рассылка c id = {$mailing->id}");
        }

        $collectionUser = R::findCollection("users", "LIMIT 100 OFFSET {$mailing->offset}");

        while ($user = $collectionUser->next()) {
            array_push($users_for_mailing, $user->telegram_id);
        }

        $mailing_complete = sendingAll($users_for_mailing, $mailing->message);

        if ($mailing_complete) {
            $mailing->offset = $mailing->offset + 100;
            R::store($mailing);
        }

    } else {
        logFile("Нет не законченых рассылок", ACCESS_LOG);
        exit("Нет не законченых рассылок");
    }

}

if ($db_connected) {
    R::close();
}
