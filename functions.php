<?php
date_default_timezone_set("Asia/Almaty");

$options_f = file('.config');
$options = array();
foreach ($options_f as $opt) {
    $options[explode("=", $opt)[0]] = trim(explode("=", $opt)[1]);
}
unset($options_f);

function logFile($s, $type_log)
{
    $file = __DIR__ . "/logs/$type_log.log";
    $currentDate = date("d.m.Y H:i:s");
    $text = sprintf("[%s] - %s", $currentDate, $s) . PHP_EOL;
    $fOpen = fopen($file, 'a');
    fwrite($fOpen, $text);
    fclose($fOpen);
}

function getKineuBotToken()
{
    global $options;
    return $options['TOKEN'];
}

function getDatabaseConnectData()
{
    global $options;
    return array("host" => $options["HOST"], "dbname" => $options["DATABASE"], 'dbUser' => $options["DBUSER"], 'dbUserPass' => $options["DBUSERPASS"]);
}

function getBotCommand($s, $o, $l)
{
    return mb_substr($s, $o + 1, $l - 1, "UTF-8"); // возвращаем только само слово комманды, без слэша и всего остального
}

function checkNumberText($s)
{
    return ctype_digit($s);
}

function isJson($string)
{
    return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
}

function TimeStamptoDateTime($unixTimestamp)
{
    return date("Y-m-d H:i:s", $unixTimestamp);
}
