<?php

require_once "questionAndAnswer.php";

function runCommand($c)
{
    $function_name = $c . "Command";
    if (function_exists($function_name)) {
        logFile("Вызывается функция $function_name", ACCESS_LOG);
        call_user_func($function_name);
    } else {
        logFile("Требуется вызов функции $function_name, но данная функция ещё не определена.", ERROR_LOG);
    }
}

function iDontKnowCommand()
{
    global $kineuBot;
    global $chat_id;
    global $userData;

    $kineuBot->sendMessage([
        'chat_id' => $chat_id,
        'text' => 'К сожалению в моей базе нет ответов на Ваше сообщение! 😞',
    ]);

    $user = R::findOneOrDispense('users', "telegram_id = {$userData['id']}");

    $user_message = R::dispense("usermessages");
    $user_message->message = $kineuBot->getWebhookUpdates()->getMessage()->getText();
    $user_message->date = TimeStamptoDateTime($kineuBot->getWebhookUpdates()->getMessage()->getDate());

    $user->ownUsermessagesList[] = $user_message;

    R::store($user);

    logFile("Прислано сообщение на которое не созданы никакие реакции в коде", ERROR_LOG);
}

function startCommand()
{
    global $kineuBot;
    global $chat_id;
    global $userData;

    $keyboard = [
        ['Я хочу узнать об Университете'],
        ['Я хочу узнать о Колледже'],
    ];

    $reply_markup = $kineuBot->replyKeyboardMarkup([
        'keyboard' => $keyboard,
        'resize_keyboard' => true,
        'one_time_keyboard' => true,
    ]);

    $response = $kineuBot->sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Пожалуйста выберите один из пунктов меню!',
        'reply_markup' => $reply_markup,
    ]);

    $user = R::findOne('users', "telegram_id = {$userData['id']}");
    $user->current_menu = "main";
    $updateCurrentUserData = R::store($user);

    logFile("Выводим главное меню", ACCESS_LOG);

}

function mainMenuCommand()
{
    global $kineuBot;
    global $chat_id;
    global $userData;

    $keyboard = [
        ['Я хочу узнать об Университете'],
        ['Я хочу узнать о Колледже'],
    ];

    $reply_markup = $kineuBot->replyKeyboardMarkup([
        'keyboard' => $keyboard,
        'resize_keyboard' => true,
        'one_time_keyboard' => true,
    ]);

    $response = $kineuBot->sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Пожалуйста выберите один из пунктов меню!',
        'reply_markup' => $reply_markup,
    ]);

    $user = R::findOne('users', "telegram_id = {$userData['id']}");
    $user->current_menu = "main";
    $updateCurrentUserData = R::store($user);

    logFile("Выводим главное меню", ACCESS_LOG);

}

function menuLv2CollegeCommand()
{
    global $kineuBot;
    global $chat_id;
    global $userData;
    global $CollegeQuestionList;
    $CollegeQuestionList = implode(PHP_EOL, $CollegeQuestionList);

    $keyboard = [
        ['1', '2', '3', '4', '5', '6'],
        ['Назад'],
    ];

    $reply_markup = $kineuBot->replyKeyboardMarkup([
        'keyboard' => $keyboard,
        'resize_keyboard' => true,
        'one_time_keyboard' => false,
    ]);

    $response = $kineuBot->sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Мы можем ответить на один из приведённых ниже вопросов, пожалуйста нажмите кнопку соответствующею номеру вопроса:' . PHP_EOL . $CollegeQuestionList,
        'reply_markup' => $reply_markup,
    ]);

    logFile("Выводим меню для колледжа", ACCESS_LOG);

    $user = R::findOne('users', "telegram_id = {$userData['id']}");
    $user->current_menu = "second_college";
    $updateCurrentUserData = R::store($user);

    logFile("Записываем на каком уровне меню данный пользователь", ACCESS_LOG);

}

function menuLv2UniversityCommand()
{
    global $kineuBot;
    global $chat_id;
    global $userData;
    global $UniversityQuestionList;
    $UniversityQuestionList = implode(PHP_EOL, $UniversityQuestionList);

    $keyboard = [
        ['1', '2', '3', '4', '5', '6'],
        ['Назад'],
    ];

    $reply_markup = $kineuBot->replyKeyboardMarkup([
        'keyboard' => $keyboard,
        'resize_keyboard' => true,
        'one_time_keyboard' => false,
    ]);

    $response = $kineuBot->sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Мы можем ответить на один из приведённых ниже вопросов, пожалуйста нажмите кнопку соответствующею номеру вопроса:' . PHP_EOL . $UniversityQuestionList,
        'reply_markup' => $reply_markup,
    ]);

    logFile("Выводим меню для универа", ACCESS_LOG);

    $user = R::findOne('users', "telegram_id = {$userData['id']}");
    $user->current_menu = "second_university";
    $updateCurrentUserData = R::store($user);

    logFile("Записываем на каком уровен меню данный пользователь", ACCESS_LOG);

}

function getAnswer($ans_number)
{
    global $kineuBot;
    global $chat_id;
    global $userData;

    $user_menu_context = R::findAndExport('users', "telegram_id = {$userData['id']}")[0]['current_menu']; // получаем контекст меню пользователя

    switch ($user_menu_context) {
        case 'second_university':
            global $UniversityAnswerList;
            global $UniversityQuestionList;
            $answer_list = $UniversityAnswerList;
            $question_list = $UniversityQuestionList;
            break;
        case 'second_college':
            global $CollegeAnswerList;
            global $CollegeQuestionList;
            $answer_list = $CollegeAnswerList;
            $question_list = $CollegeQuestionList;
            break;
        default:
            # code...
            break;
    }

    $ans_number = (int) $ans_number - 1; // номер вопроса на который нужен ответ с учётом нумерации с нуля
    $answersCount = count($answer_list) - 1; // количество ответов в масиве с учётом нумерации с нуля

    if ($ans_number > $answersCount) { // если пользователь прислал номер больший чем есть ответов в масиве, то говорим ему об этом
        $kineuBot->sendMessage([
            'chat_id' => $chat_id,
            'text' => 'У нас в базе нет вопроса под таким номером. :(',
        ]);
    } else { // Отвечаем пользователю, прикладывая вопрос и ответ
        $kineuBot->sendMessage([
            'chat_id' => $chat_id,
            'text' => "Вы спросили у нас:*" . PHP_EOL . $question_list[$ans_number] . "*" . PHP_EOL . "Ответ на Ваш вопрос:" . PHP_EOL . $answer_list[$ans_number],
            'parse_mode' => "Markdown",
            'disable_web_page_preview' => false,
        ]);
    }

    logFile("Выводим ответ на вопрос пользователя", ACCESS_LOG);

}
